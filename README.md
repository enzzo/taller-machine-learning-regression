[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/emilopez%2Ftaller-machine-learning-regression/HEAD)

# Taller Machine Learning Regression

- Videoconferencia: https://meet.jit.si/charla-de-bar-con-ML20210219

## Temas

- Introducción / Presentación del problema
- Análisis exploratorio
    - gráficos de dispersión
    - histogramas
    - box plots

- Regresión Lineal Múltiple (RLM)
- ¿Evaluación?
- K-Nearest Neighbor (KNN)
- Random Forest (RFO)
- Suport Vector Regression (SVR)